class Post < ApplicationRecord
  include Bootsy::Container
  mount_uploader :image, Bootsy::ImageUploader

  mount_uploader :main_image, ArticleImageUploader
end
